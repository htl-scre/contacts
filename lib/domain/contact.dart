class Contact {
  String? id;
  String name;
  String phoneNumber;

  Contact(this.id, this.name, this.phoneNumber);

  Contact.fromJson(String id, Map<String, dynamic> json)
      : name = json['name'],
        phoneNumber = json['phoneNumber'];

  Map<String, dynamic> get json {
    return {
      "name": name,
      "phoneNumber": phoneNumber,
    };
  }

  @override
  String toString() {
    return 'Contact{name: $name, phoneNumber: $phoneNumber}';
  }
}
