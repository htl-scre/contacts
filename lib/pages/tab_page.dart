import 'package:contacts/pages/contacts_overview_page.dart';
import 'package:contacts/pages/create_contact_page.dart';
import 'package:flutter/material.dart';

class TabPage extends StatefulWidget {
  const TabPage({Key? key}) : super(key: key);

  @override
  State<TabPage> createState() => _TabPageState();
}

class _TabPageState extends State<TabPage> {
  static final _pages = [
    ContactsOverviewPage(),
    CreateContactPage(),
  ];

  int _selectedPageIndex = 0;

  void _selectPage(int index) {
    setState(() => _selectedPageIndex = index);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _pages[_selectedPageIndex],
        bottomNavigationBar: BottomNavigationBar(
          onTap: _selectPage,
          currentIndex: _selectedPageIndex,
          items: const [
            BottomNavigationBarItem(
                label: 'Contacts', icon: Icon(Icons.contacts)),
            BottomNavigationBarItem(label: 'Add', icon: Icon(Icons.add)),
            BottomNavigationBarItem(label: 'Add2', icon: Icon(Icons.add)),
          ],
        ));
  }
}
