import 'package:contacts/domain/contact.dart';
import 'package:contacts/pages/create_contact_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/contact_provider.dart';

class ContactsOverviewPage extends StatefulWidget {
  static const routeName = 'contacts';

  const ContactsOverviewPage({Key? key}) : super(key: key);

  @override
  State<ContactsOverviewPage> createState() => _ContactsOverviewPageState();
}

class _ContactsOverviewPageState extends State<ContactsOverviewPage> {
  late ContactProvider provider;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    provider = Provider.of<ContactProvider>(context, listen: true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contacts'),
      ),
      body: FutureBuilder(
        future: provider.loadContacts(),
        builder: (_, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          }
          if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error.toString()}'));
          }
          if (snapshot.hasData) {
            return buildListView(provider.contacts);
          }
          throw 'Unexpected';
        },
      ),
      // child: buildListView(contacts)
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.of(context)
            .pushNamed(CreateContactPage.routeName, arguments: 'args'),
        child: Icon(Icons.add),
      ),
    );
  }

  ListView buildListView(List<Contact> contacts) {
    return ListView.builder(
      itemBuilder: (_, index) {
        return Card(
          elevation: 5,
          child: ListTile(
            leading: Icon(Icons.contact_phone),
            title: Text(contacts[index].name),
            subtitle: Text(contacts[index].phoneNumber),
          ),
        );
      },
      itemCount: contacts.length,
    );
  }
}
