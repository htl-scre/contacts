import 'package:contacts/domain/contact.dart';
import 'package:contacts/providers/contact_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CreateContactPage extends StatelessWidget {
  static const routeName = 'create-contact';

  var _key = GlobalKey<FormState>();
  var contactDto = ContactDto();

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<ContactProvider>(context, listen: false);
    return Scaffold(
        appBar: AppBar(title: Text('Add contact')),
        body: Form(
          key: _key,
          child: Column(
            children: [
              TextFormField(
                decoration: InputDecoration(labelText: 'Name'),
                keyboardType: TextInputType.name,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Required';
                  }
                  return null;
                },
                onSaved: (newValue) => contactDto.name = newValue!,
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Phone Number'),
                keyboardType: TextInputType.phone,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Required';
                  }
                  return null;
                },
                onSaved: (newValue) => contactDto.phoneNumber = newValue!,
              ),
              ElevatedButton(
                onPressed: () {
                  if (_key.currentState!.validate()) {
                    _key.currentState?.save();
                    var contact = contactDto.toContact();
                    provider.addContact(contact);
                    Navigator.of(context).pop();
                  }
                },
                child: Icon(Icons.add),
              )
            ],
          ),
        ));
  }
}

class ContactDto {
  String? name;
  String? phoneNumber;

  ContactDto({this.name, this.phoneNumber});

  Contact toContact() {
    if (name == null || phoneNumber == null) {
      throw 'Cannot convert';
    }
    return Contact(null, name!, phoneNumber!);
  }
}
