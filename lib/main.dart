import 'package:contacts/pages/contacts_overview_page.dart';
import 'package:contacts/pages/create_contact_page.dart';
import 'package:contacts/pages/tab_page.dart';
import 'package:contacts/providers/contact_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const ContactsApp());
}

class ContactsApp extends StatelessWidget {
  const ContactsApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => ContactProvider(),
      child: MaterialApp(
        home: const TabPage(),
        routes: {
          CreateContactPage.routeName: (_) => CreateContactPage(),
          ContactsOverviewPage.routeName: (_) => ContactsOverviewPage(),
        },
      ),
    );
  }
}
