import 'dart:convert';

import 'package:contacts/domain/contact.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ContactProvider with ChangeNotifier {
  List<Contact> _contacts = [];
  static const url =
      'https://contacts-e6169-default-rtdb.europe-west1.firebasedatabase.app/contacts.json'; // .json wegen firebase

  List<Contact> get contacts {
    return _contacts;
  }

  Future<List<Contact>> loadContacts() async {
    if (_contacts.isNotEmpty) {
      return [];
    }
    var response = await http.get(Uri.parse(url));
    var contacts = jsonDecode(response.body) as Map<String, dynamic>;
    for (var contactData in contacts.entries) {
      var contact = Contact.fromJson(contactData.key, contactData.value);
      _contacts.add(contact);
    }
    notifyListeners();
    return _contacts;
  }

  void addContact(Contact contact) async {
    var response =
        await http.post(Uri.parse(url), body: jsonEncode(contact.json));
    var json = jsonDecode(response.body) as Map<String, dynamic>;
    contact.id = json['name'];
    _contacts.add(contact);
    notifyListeners();
  }
}
